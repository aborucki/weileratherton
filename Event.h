#pragma once
#include "DefsCV.h"
#include "BOLine.h"
#include "IntersectionPoint.h"

using namespace std;

struct Event
{
	IntersectionPoint point;
	BOLine* line;
	BOLine* line2;
	BOEventType eventType;

	Event()
	{
	}

	Event(IntersectionPoint point, BOLine* line, BOLine* line2, BOEventType eventType)
	{
		this->point = point;
		this->line = line;
		this->line2 = line2;
		this->eventType = eventType;
	}

	friend ostream & operator<< (ostream &out, const Event &e)
	{
		return out << e.point << "---" << *(e.line);
	}
};
