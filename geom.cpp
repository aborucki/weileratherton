﻿#include "geom.h"
#include <algorithm>    // std::max

using namespace std; // max

/*
równanie prostej przechodzącej przez punkty A(x0, y0) i B(x1, y1) :
*/
void line_normal_form(const Line &line, double &A, double &B, double &C)
{
	A = line.y1 - line.y0;
	B = line.x0 - line.x1;
	C = line.x1*line.y0 - line.x0*line.y1;
}


/*
Punkt przecięcia A0x + B0y + C0 = 0 i A1x + B1y + C1 = 0 jest:
mianownik: A0B1-A1B0<>0; x=(B0C1-B1C0)/mianownik; y = - (A0C1-A1C0)/mianownik
*/
bool lines_intersection(const Line &line0, const Line &line1, double &fx, double &fy)
{
	const double Eps = 1e-10;
	double A0, B0, C0;
	double A1, B1, C1;
	line_normal_form(line0, A0, B0, C0);
	line_normal_form(line1, A1, B1, C1);
	double denominator = A0*B1 - A1*B0;
	if (fabs(denominator)<Eps) return false;
	fx = (B0*C1 - B1*C0) / denominator;
	fy = -(A0*C1 - A1*C0) / denominator;
	return true;
}

//sprawdza czy x jest pomiędzy a i b, niezależnie które z nich jest większe
bool between(double x, double a, double b)
{
	if (a <= b)
		return x >= a && x <= b;
	else
		return x >= b && x <= a;
}

/*
Sprawdza czy przecina się linia prosta z odcinkiem
linia przechodzi cx,cy i ma nachylenie kx,ky
*/
bool line_segment_intersection(int cx, int cy, int kx, int ky, const Line &segment, double &fx, double &fy)
{
	Line line;
	line.x0 = cx;
	line.y0 = cy;
	line.x1 = cx + kx;
	line.y1 = cy + ky;
	if (!lines_intersection(line, segment, fx, fy)) return false;
	return between(fx, segment.x0, segment.x1) && between(fy, segment.y0, segment.y1);
}

double det_matrix(const Line &seg, Point z)
{
	return((double)seg.x0*seg.y1 + (double)seg.x1*z.y + (double)z.x*seg.y0 - (double)z.x*seg.y1 - (double)seg.x0*z.y - (double)seg.x1*seg.y0);
}

//Ta funkcja sprawdza, czy punkt z należy do odcinka |xy|
bool point_in_segment(const Line &seg, Point z)
{
	double det; //wyznacznik macierzy
	det = det_matrix(seg, z);
	if (fabs(det) > 1e-6) return(false); else
	{
		if (
			(min(seg.x0, seg.x1) <= z.x) && (z.x <= max(seg.x0, seg.x1)) && (min(seg.y0, seg.y1) <= z.y) && (z.y <= max(seg.y0, seg.y1))
			)
			return(true); else
			return(false);
	}
}

bool segment_segment_intersection(const Line &segment0, const Line &segment1, double &fx, double &fy)
{
	int minyseg0, maxyseg0;
	int minyseg1, maxyseg1;
	if (segment0.y1 > segment0.y0)
	{
		minyseg0 = segment0.y0;
		maxyseg0 = segment0.y1;
	}
	else
	{
		minyseg0 = segment0.y1;
		maxyseg0 = segment0.y0;
	}
	if (segment1.y1 > segment1.y0)
	{
		minyseg1 = segment1.y0;
		maxyseg1 = segment1.y1;
	}
	else
	{
		minyseg1 = segment1.y1;
		maxyseg1 = segment1.y0;
	}
	if (maxyseg0 < minyseg1) return false;
	if (maxyseg1 < minyseg0) return false;

	int minxseg0, maxxseg0;
	int minxseg1, maxxseg1;
	if (segment0.x1 > segment0.x0)
	{
		minxseg0 = segment0.x0;
		maxxseg0 = segment0.x1;
	}
	else
	{
		minxseg0 = segment0.x1;
		maxxseg0 = segment0.x0;
	}
	if (segment1.x1 > segment1.x0)
	{
		minxseg1 = segment1.x0;
		maxxseg1 = segment1.x1;
	}
	else
	{
		minxseg1 = segment1.x1;
		maxxseg1 = segment1.x0;
	}
	//if (maxxseg0 < minxseg1) return false;
	//if (maxxseg1 < minxseg0) return false;
	if (max(segment0.x0, segment0.x1) < min(segment1.x0, segment1.x1)) return false;
	if (max(segment1.x0, segment1.x1) < min(segment0.x0, segment0.x1)) return false;
	//Sprawdzanie, czy jakiś punkt należy do drugiego odcinka
	if (point_in_segment(segment0, Point(segment1.x0, segment1.y0)) == true)
	{
		fx = segment1.x0, fy = segment1.y0;
		return true;
	}
	else
	if (point_in_segment(segment0, Point(segment1.x1, segment1.y1)) == true)
	{
		fx = segment1.x1, fy = segment1.y1;
		return true;
	}
	else
	if (point_in_segment(segment1, Point(segment0.x0, segment0.y0)) == true)
	{
		fx = segment0.x0, fy = segment0.y0;
		return true;
	}
	else
	if (point_in_segment(segment1, Point(segment0.x1, segment0.y1)) == true)
	{
		fx = segment0.x1, fy = segment0.y1;
		return true;
	}
	else
		//zaden punkt nie nalezy do drugego odcinka
	if (
		(det_matrix(segment0, Point(segment1.x0, segment1.y0)))*(det_matrix(segment0, Point(segment1.x1, segment1.y1))) >= 0
		) return false;
	else if (
		(det_matrix(segment1, Point(segment0.x0, segment0.y0)))*(det_matrix(segment1, Point(segment0.x1, segment0.y1))) >= 0
		) return false;
	else //znaki wyznaczników sa równe
	{
		lines_intersection(segment0, segment1, fx, fy);
		return true;
	}
}

// algorytm znaleziony w http://www.efg2.com/Lab/Graphics/PolygonArea.htm
// napisany oryginalnie w Delphi
// wylicza pole każdego wielokąta, nie tylko wypukłego
// dla wielokąta z 1 i 2 punktów zwraca zero

// Find the area of a polygon using a method based on Green's theorem.
//
// The Polyg array is assumed to be closed, i.e.,
// Polyg[0] = Polyg[High(Polyg)]  (thanks to Gary Williams for correction)
//
// The algebraic sign of the area is positive for counterclockwise
// ordering of vertices in the X-Y plane, and negative for
// clockwise ordering.
//
// Reference:  "Centroid of a Polyg" in Graphics Gems IV,
// by Gerard Bashein and Paul R. Detmer,
// Paul S. Heckbert (editor), Academic Press, 1994, pp. 3-6.

double PolygonArea(Poly *poly)
{
 Point p,nextp;
	int n = poly->n;
	double sum = 0;
	for (int i=0; i<n; i++)
	{
		p = poly->pts[i];
		if (i+1<n)
			nextp = poly->pts[i+1];
		else
			nextp = poly->pts[0];
		sum += p.x * nextp.y - nextp.x * p.y;
	}
	return fabs(0.5 * sum);
}
