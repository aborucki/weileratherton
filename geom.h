#pragma once
#include "common.h"

double det_matrix(const Line &seg, Point z);
bool segment_segment_intersection(const Line &segment0, const Line &segment1, double &fx, double &fy);
double PolygonArea(Poly *poly);