//plik b�dzie niepotrzsebny przy korzystaniu z OpenCV
#pragma once
#define USE_OPENCV

#include "BOGlobal.h"

#ifdef USE_OPENCV

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/types_c.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/highgui/highgui_c.h"
#include "opencv2/core/core.hpp"

using namespace cv;

#else
#include <cmath>
#include <ostream>

using std::ostream;

struct Point
{
	int x, y;
	Point()
	{
		x = 0; y = 0;
	}

	Point(int x, int y)
	{
		this->x = x;
		this->y = y;
	}
};

struct Point2d
{
	double x;
	double y;

	Point2d()
	{
		x = 0; y = 0;
	}

	Point2d(double x, double y)
	{
		this->x = x;
		this->y = y;
	}

	Point2d operator+ (Point2d &p)
	{
		return Point2d(x + p.x, y + p.y);
	}

	Point2d operator- (Point2d &p)
	{
		return Point2d(x + p.x, y + p.y);
	}

	Point2d operator* (double d)
	{
		return Point2d(d * x, d * y);
	}

	int operator==(const Point2d &p)
	{
		return EQ(x, p.x) && EQ(y, p.y);
	}

	int operator!=(const Point2d &p)
	{
		return !EQ(x, p.x) || !EQ(y, p.y);
	}

	friend ostream & operator<< (ostream &out, const Point2d &p)
	{
		return out << "[" << p.x << ", " << p.y << "]";
	}
};
#endif /* USE_OPENCV*/