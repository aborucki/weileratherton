#pragma once
#include "DefsCV.h"
#include "common.h"

void scalePoints(Point &p0, Point &p1);
void drawPoly(Mat mat, Poly *polygon, Scalar color);