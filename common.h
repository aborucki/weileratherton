#pragma once
#include "DefsCV.h"
#include <algorithm>    // std::max
#include <vector>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std; // max

struct Line
{
	int x0, y0, x1, y1;
	Line()
	{
		Line(0, 0, 0, 0);
	}

	Line(int _x0, int _y0, int _x1, int _y1)
	{
		x0 = _x0;
		y0 = _y0;
		x1 = _x1;
		y1 = _y1;
	}
};

class Poly
{
public:
	int n;
	Point *pts;
	//konstruktor domy�lny
	Poly()
	{
		this->n = 0;
		pts = NULL;
	}
	void SetSize(int n)
	{
		int old_n = this->n;
		this->n = n;
		Point *oldpts = pts;
		pts = new Point[n];
		if (oldpts != NULL)
		{
			for (int i = 0; i < min(n, old_n); i++)
				pts[i] = oldpts[i];
			delete oldpts;
		}
	}
	Poly(int n)
	{
		this->n = 0;
		pts = NULL;
		SetSize(n);
	}
	Poly(const Poly &cyto)
	{
		int i;
		this->n = 0;
		pts = NULL;
		SetSize(cyto.n);
		for (i = 0; i<n; i++)
		{
			pts[i] = cyto.pts[i];
		}
	}

	~Poly()
	{
		delete[]pts;
	}
};


void toClockwise(Poly *polyIn, Poly *polyOut, bool bAnti, long long &sum);
