#pragma once

#include <cmath>
#include "Event.h"

class Compare
{
public:
	bool operator() (Event event1, Event event2)
	{
		if (EQ(event1.point.x, event2.point.x))
		{
			return event1.point.y > event2.point.y;
		}
		else
		{
			return event1.point.x > event2.point.x;
		}
	}
};