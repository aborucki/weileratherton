#pragma  once
#include <iostream>
#include "BOLine.h"
using namespace std;

struct BOLine;

struct Node
{
	Node* parent; // rodzic
	Node* left; // lewe dziecko
	Node* right; // prawe dziecko
	BOLine* pValue; // wartosc
	int balanceFactor; // wspolczynnik wywazenia
};

class AVL
{
private:

	Node* root; // korzen


	// rotacje
	Node* RR(Node* node);
	Node* LL(Node* node);
	Node* RL(Node* node);
	Node* LR(Node* node);

	void Insert(Node* node);



	Node* maxNode(Node* x);
	Node* minNode(Node* x);


	Node* Remove(Node* node);
	// poprzednia wartosc (jesli chodzi o porzadek)


	void Print(ostream& out, Node* node) const;
	void Clear(Node *node); //wo�ane tylko z destruktora
public:

	AVL();
	~AVL();

	Node* Insert(BOLine* pValue);
	bool Find(BOLine* pValue);
	Node* Find(BOLine* pValue, bool r);

	void Remove(BOLine* pValue);

	friend ostream & operator<<(ostream &ost, const AVL &avl);

	Node* pred(Node* x);
	Node* succ(Node* x);
};
