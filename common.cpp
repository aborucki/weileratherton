#include "common.h"

//wed�ug http://stackoverflow.com/questions/1165647/how-to-determine-if-a-list-of-polygon-points-are-in-clockwise-order
//przez Beta
//dodane: eliminacja odcink�w d�ugo�ci zero
void toClockwise(Poly *polyIn, Poly *polyOut, bool bAnti, long long &sum)
{
	sum = 0;
	int cntZeroLen = 0;
	for (int i = 0; i < polyIn->n; i++)
	{
		Point P0 = polyIn->pts[i];
		Point P1;
		if (i<polyIn->n - 1)
			P1 = polyIn->pts[i + 1];
		else
			P1 = polyIn->pts[0];
		if (P0.x == P1.x && P0.y == P1.y)
			cntZeroLen++;
		sum += (P1.x - P0.x)*(P1.y + P0.y);
	}
	Poly tmpPoly;
	int newSize = polyIn->n - cntZeroLen;
	tmpPoly.SetSize(newSize);
	Point prevp, p0;
	int ii = 0;
	for (int i = 0; i < polyIn->n; i++)
	{
		Point p = polyIn->pts[i];
		if (i == 0) p0 = p;
		if ((i == 0) || (prevp.x != p.x || prevp.y != p.y))
		{
			if (ii<newSize)
			{
				tmpPoly.pts[ii] = polyIn->pts[i];
				ii++;
				prevp = p;
			}
			else
				assert(p.x == p0.x && p.y == p0.y);
		}
	}
	assert(ii + cntZeroLen == polyIn->n);
	polyOut->SetSize(tmpPoly.n);
	bool mustReverse;
	if (bAnti)
		mustReverse = sum < 0;
	else
		mustReverse = sum > 0;
	if (mustReverse)
	{//odwr�cenie kolejno�ci
		int n = tmpPoly.n;
		for (int i = 0; i < tmpPoly.n; i++)
		{
			polyOut->pts[n - 1 - i] = tmpPoly.pts[i];
		}
	}
	else
	{ //zwyk�e kopiowanie
		for (int i = 0; i < tmpPoly.n; i++)
		{
			polyOut->pts[i] = tmpPoly.pts[i];
		}
	}
}
