#include "AVL.h"


AVL::AVL()
{
	root = nullptr;
}

AVL::~AVL()
{
	Clear(root);
}

void AVL::Clear(Node *node)
{
	if (node != NULL)
	{
		Clear(node->left);
		Clear(node->right);
		delete node;
	}
}

Node* AVL::Insert(BOLine* pValue)
{
	cout << "Insert " << *pValue << endl;
	Node* node = new Node();
	node->pValue = pValue;
	Insert(node);
	return node;
}

// rotacja w prawo
Node* AVL::RR(Node* node)
{
	Node* x = node->right;
	Node* p = node->parent;
	node->right = x->left;

	if (node->right)
	{
		node->right->parent = node;
	}

	x->left = node;
	x->parent = p;
	node->parent = x;

	if (p)
	{
		if (p->left == node)
		{
			p->left = x;
		}
		else
		{
			p->right = x;
		}
	}
	else
	{
		root = x;
	}

	if (x->balanceFactor == -1)
	{
		node->balanceFactor = x->balanceFactor = 0;
	}
	else
	{
		node->balanceFactor = -1;
		x->balanceFactor = 1;
	}
	return x;
}

// rotacja w lewo
Node* AVL::LL(Node* node)
{
	Node* x = node->left;
	Node* p = node->parent;
	node->left = x->right;

	if (node->left)
	{
		node->left->parent = node;
	}

	x->right = node;
	x->parent = p;
	node->parent = x;


	if (p)
	{
		if (p->left == node)
		{
			p->left = x;
		}
		else
		{
			p->right = x;
		}
	}
	else
	{
		root = x;
	}

	if (x->balanceFactor == 1)
	{
		node->balanceFactor = x->balanceFactor = 0;
	}
	else
	{
		node->balanceFactor = 1;
		x->balanceFactor = -1;
	}

	return x;
}


// rotacja prawo-lewo
Node* AVL::RL(Node* node)
{
	Node* x = node->right;
	Node* y = x->left;
	Node* p = node->parent;
	x->left = y->right;

	if (x->left)
	{
		x->left->parent = x;
	}

	node->right = y->left;

	if (node->right)
	{
		node->right->parent = node;
	}

	y->left = node;
	y->right = x;
	node->parent = x->parent = y;
	y->parent = p;


	if (p)
	{
		if (p->left == node)
		{
			p->left = y;
		}
		else
		{
			p->right = y;
		}
	}
	else
	{
		root = y;
	}

	node->balanceFactor = y->balanceFactor == -1 ?  1 : 0;
	x->balanceFactor = y->balanceFactor ==  1 ? -1 : 0;
	y->balanceFactor = 0;

	return y;
}

// rotacje lewo-prawo
Node* AVL::LR(Node* node)
{
	Node* x = node->left;
	Node* y = x->right;
	Node* p = node->parent;

	x->right = y->left;

	if (x->right)
	{
		x->right->parent = x;
	}


	node->left = y->right;

	if (node->left)
	{
		node->left->parent = node;
	}

	y->right = node;
	y->left = x;

	node->parent = x->parent = y;
	y->parent = p;

	if (p)
	{
		if (p->left == node)
		{
			p->left = y;
		}
		else
		{
			p->right = y;
		}
	}
	else
	{
		root = y;
	}

	node->balanceFactor = y->balanceFactor ==  1 ? -1 : 0;
	x->balanceFactor = y->balanceFactor == -1 ?  1 : 0;
	y->balanceFactor = 0;

	return y;
}


void AVL::Insert(Node* node)
{
	Node* x = root;
	Node* y;
	Node* z;

	y = nullptr;

	node->left = nullptr;
	node->right = nullptr;
	node->balanceFactor = 0;

	while (x)
	{
		y = x;

		if (*node->pValue < *x->pValue)
		{
			x = x->left;
		}
		else
		{
			x = x->right;
		}
	}

	node->parent = y;

	if (!y)
	{
		root = node;
		return;
	}

	if (*node->pValue < *y->pValue)
	{
		y->left = node;
	}
	else
	{
		y->right = node;
	}

	if (y->balanceFactor)
	{
		y->balanceFactor = 0;
		return;
	}

	y->balanceFactor = (y->left == node) ? 1 : -1;
	z = y->parent;

	while (z)
	{

		if (z->balanceFactor)
		{
			break;
		}

		z->balanceFactor = z->left == y ? 1 : -1;

		y = z;
		z = z->parent;
	}

	if (!z) return;

	if (z->balanceFactor == 1)
	{
		if (z->right == y)
		{
			z->balanceFactor = 0;
			return;
		}

		if (y->balanceFactor == -1)
		{
			LR(z);
		}
		else
		{
			LL(z);
		}

		return;
	}
	else
	{
		if (z->left == y)
		{
			z->balanceFactor = 0;
			return;
		}
		if (y->balanceFactor == 1)
		{
			RL(z);
		}
		else
		{
			RR(z);
		}
		return;
	}
}

bool AVL::Find(BOLine* pValue)
{
	return Find(pValue, true) != nullptr;
}

Node* AVL::Find(BOLine* pValue, bool ret)
{
	Node* node = root;

	while (node && node->pValue != pValue)
	{
		if (*pValue < *node->pValue)
		{
			node = node->left;
		}
		else
		{
			node = node->right;
		}
	}


	return node;
}


void AVL::Remove(BOLine* pValue)
{
	cout << "Remove " << *pValue << endl;
	Node* node = Find(pValue, true);

	if (node)
	{
		Remove(node);
		delete node;
	}
	else
	{
		cout << "----------------------NOT FOUND" << endl;
	}
}

Node* AVL::Remove(Node* node)
{
	Node* x;
	Node* y;
	Node* z;

	bool b;

	if (node->left && node->right)
	{
		y = Remove(pred(node));
		b = false;
	}
	else
	{
		if (node->left)
		{
			y = node->left;
			node->left = nullptr;
		}
		else
		{
			y = node->right;
			node->right = nullptr;
		}

		node->balanceFactor = 0;
		b = true;

	}

	if (y)
	{
		y->parent = node->parent;

		y->left = node->left;
		y->right = node->right;

		if (y->left)
		{
			y->left->parent = y;
		}

		if (y->right)
		{
			y->right->parent = y;
		}

		y->balanceFactor = node->balanceFactor;
	}

	if (node->parent)
	{
		if (node->parent->left == node)
		{
			node->parent->left = y;
		}
		else
		{
			node->parent->right = y;
		}
	}
	else
	{
		root = y;
	}

	if (b)
	{
		z = y;
		y = node->parent;

		while (y)
		{
			if (!y->balanceFactor)
			{
				if (y->left == z)
				{
					y->balanceFactor = -1;
				}

				else
				{
					y->balanceFactor = 1;
				}

				break;
			}
			else
			{
				bool c1 = y->balanceFactor ==  1 && y->left  == z;
				bool c2 = y->balanceFactor == -1 && y->right == z;

				if (c1 || c2)
				{
					y->balanceFactor = 0;


					z = y;
					y = y->parent;
				}
				else
				{
					if (y->left == z)
					{
						x = y->right;
					}

					else
					{
						x = y->left;
					}

					if (!x->balanceFactor)
					{
						if (y->balanceFactor == 1)
						{
							LL(y);
						}
						else
						{
							RR(y);
						}
						break;
					}

					else if (y->balanceFactor == x->balanceFactor)
					{
						if (y->balanceFactor == 1)
						{
							LL(y);
						}
						else
						{
							RR(y);
						}

						z = x;
						y = x->parent;
					}

					else
					{
						if (y->balanceFactor == 1)
						{
							LR(y);
						}
						else
						{
							RL(y);
						}


						z = y->parent;
						y = z->parent;
					}
				}
			}
		}
	}
	return node;
}

// Zwraca w�ze� z maksymalnym kluczem
//-----------------------------------

Node * AVL::minNode(Node * x)
{
	while(x->left) x = x->left;
	return x;
}

// Zwraca w�ze� z minimalnym kluczem
//----------------------------------

Node * AVL::maxNode(Node * x)
{
	while(x->right) x = x->right;
	return x;
}


Node * AVL::pred(Node * x)
{
	Node* t = x;
	if(x->left) return maxNode(x->left);

	Node * y;

	do
	{
		y = x;
		x = x->parent;
	} while(x && (x->right != y));


	if (t == x)
	{
		//return NULL;
	}
	return x;
}

// Zwraca w�ze� nast�pnika
//------------------------

Node * AVL::succ(Node * x)
{
	Node* t = x;
	if(x->right) return minNode(x->right);

	Node * y;

	do
	{
		y = x;
		x = x->parent;
	} while(x && (x->left != y));

	if (t == x)
	{
		//return NULL;
	}
	return x;
}


void AVL::Print(ostream & out, Node* node) const
{
	if (node)
	{
		Print(out, node->left);
		out << *(node->pValue) << "\n";
		Print(out, node->right);
	}
}

ostream & operator<<(ostream &ost, const AVL &avl)
{
	avl.Print(ost, avl.root);
	return ost << endl;
}
