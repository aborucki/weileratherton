#include "BOGlobal.h"

bool EQ(double x, double y)
{
	return abs(x - y) < EPS;
}