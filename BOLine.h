#pragma once
#include <cmath>
#include <ostream>
#include <assert.h>

using namespace std;

#include "DefsCV.h"
#include "AVL.h"
#include "IntersectionPoint.h"

enum IntersectResult { PARALLEL, COINCIDENT, NOT_INTERESECTING, INTERESECTING };


struct Node;

struct BOLine
{
	Point2d start;
	Point2d end;
	double slope;
	Node* node;
	bool isVertical;
	int numPoly;
	int index;
	bool reversed;

	static double sweepX;

	BOLine()
	{
		BOLine(Point2d(), Point2d(), NULL, 0, 0);
	}

	double findY() const
	{
		return start.y + (sweepX - start.x) * slope;
	}


	BOLine(Point2d start, Point2d end, Node* node, int numPoly, int index)
	{
		this->start = start;
		this->end = end;

		// zamienia �eby odcinek by� z lewej do prawej
		if (start.x > end.x)
		{
			this->start = end;
			this->end = start;
			reversed = true;
		}
		else
		{
			reversed = false;
		}

		double dx = end.x - start.x;

		if (dx == 0)
		{
			isVertical = true;
			this->slope = -9999999;
		}
		else
		{
			isVertical = false;
			this->slope = (end.y - start.y) / (dx);
		}

		this->node = node;
		this->numPoly = numPoly;
		this->index = index;
	}

	int operator==(const BOLine &other)
	{
		return EQ(findY(), other.findY());// && slope == other.slope;
	}

	int operator!=(const BOLine &other)
	{
		return !EQ(findY(), other.findY());// || slope != other.slope;
	}

	int operator<(const BOLine &other)
	{
		double Y = findY();
		double otherY = other.findY();
		if (EQ(Y, otherY)) return slope < other.slope;
		return Y < otherY;
	}

	int operator>(const BOLine &other)
	{
		double Y = findY();
		double otherY = other.findY();
		if (EQ(Y, otherY)) return slope > other.slope;
		return Y > otherY;
	}



	friend ostream & operator<< (ostream &out, const BOLine &line)
	{
		double Y = line.findY();
		//return out << line.start << " -> " << line.end << " (" << Y << ", " << line.slope << ")" << "\t";
		return out << line.start << " -> " << line.end << " (" << line.numPoly << line.index << ")";
	}

	bool pointsEQ(const BOLine &line)
	{
		return start == line.start && end == line.end;
	}

	void MakeInterPoint(const BOLine& secondLine, Point2d point2d, IntersectionPoint& intersection)
	{
		assert(numPoly != secondLine.numPoly);
		assert(numPoly == 0 || numPoly == 1);
		assert(secondLine.numPoly == 0 || secondLine.numPoly == 1);
		intersection.x = point2d.x;
		intersection.y = point2d.y;
		intersection.index[numPoly] = index;
		intersection.index[secondLine.numPoly] = secondLine.index;
	}

	IntersectResult Intersect(const BOLine& secondLine, IntersectionPoint& intersection)
	{
		if (numPoly == secondLine.numPoly)
		{
			return NOT_INTERESECTING;
		}
		double denom = ((secondLine.end.y - secondLine.start.y) * (end.x - start.x)) -
			((secondLine.end.x - secondLine.start.x) * (end.y - start.y));

		double a = ((secondLine.end.x - secondLine.start.x) * (start.y - secondLine.start.y)) -
			((secondLine.end.y - secondLine.start.y)*(start.x - secondLine.start.x));

		double b = ((end.x - start.x) * (start.y - secondLine.start.y)) -
			((end.y - start.y) * (start.x - secondLine.start.x));

		Point2d S1 = start;
		Point2d E1 = end;

		Point2d S2 = secondLine.start;
		Point2d E2 = secondLine.end;

		if (reversed)
		{
			S1 = end;
			E1 = start;
		}
		if (secondLine.reversed)
		{
			S2 = secondLine.end;
			E2 = secondLine.start;
		}

		Point2d point2d;
		if (denom == 0.0f)
		{
			if (a == 0.0f && b == 0.0f)
			{
				if (S1.x >= secondLine.start.x && S1.x <= secondLine.end.x)
				{
					point2d = S1;
					MakeInterPoint(secondLine, point2d, intersection);
					if (point2d != E1 && point2d != E2)
					{
						return INTERESECTING;
					}
				}
				if (S2.x >= start.x && S2.x <= end.x)
				{
					point2d = S2;
					MakeInterPoint(secondLine, point2d, intersection);
					if (point2d != E1 && point2d != E2)
					{
						return INTERESECTING;
					}
				}
				return COINCIDENT;
			}
			return PARALLEL;
		}

		double ua = a / denom;
		double ub = b / denom;


		if(ua >= 0.0f && ua <= 1.0f && ub >= 0.0f && ub <= 1.0f)
		{
			// Get the intersection point.
			point2d.x = start.x + ua * (end.x - start.x);
			point2d.y = start.y + ua * (end.y - start.y);
			MakeInterPoint(secondLine, point2d, intersection);
			if (point2d != E1 && point2d != E2)
			{
				return INTERESECTING;
			}
		}

		return NOT_INTERESECTING;
	}
};
