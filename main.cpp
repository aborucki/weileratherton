#include <Windows.h> //QueryPerformanceCounter
#include "WeilerAtherton.h"

vector<Poly*> polygonsIn;

void sample0()
{
	Poly *polygon = new Poly(5);
	polygon->pts[0] = Point(350, 50);
	polygon->pts[1] = Point(550, 450);
	polygon->pts[2] = Point(350, 250);
	polygon->pts[3] = Point(150, 450);
	polygon->pts[4] = Point(50, 350);
	polygonsIn.push_back(polygon);

	polygon = new Poly(7);
	polygon->pts[0] = Point(400, 400);
	polygon->pts[1] = Point(250, 400);
	polygon->pts[2] = Point(100, 250);
	polygon->pts[3] = Point(100, 100);
	polygon->pts[4] = Point(600, 100);
	polygon->pts[5] = Point(500, 200);
	polygon->pts[6] = Point(500, 300);
	polygonsIn.push_back(polygon);
}

//to samo co sample0, ale zaczynanie od punktu wewnatrz
void sample1()
{
	Poly *polygon = new Poly(5);
	polygon->pts[3] = Point(350, 50);
	polygon->pts[4] = Point(550, 450);
	polygon->pts[0] = Point(350, 250);
	polygon->pts[1] = Point(150, 450);
	polygon->pts[2] = Point(50, 350);
	polygonsIn.push_back(polygon);

	polygon = new Poly(7);
	polygon->pts[0] = Point(400, 400);
	polygon->pts[1] = Point(250, 400);
	polygon->pts[2] = Point(100, 250);
	polygon->pts[3] = Point(100, 100);
	polygon->pts[4] = Point(600, 100);
	polygon->pts[5] = Point(500, 200);
	polygon->pts[6] = Point(500, 300);
	polygonsIn.push_back(polygon);
}

//to samo co sample0, ale pierwszy odwrotnie zorientowany
void sample2()
{
	Poly *polygon = new Poly(5);
	polygon->pts[4] = Point(350, 50);
	polygon->pts[3] = Point(550, 450);
	polygon->pts[2] = Point(350, 250);
	polygon->pts[1] = Point(150, 450);
	polygon->pts[0] = Point(50, 350);
	polygonsIn.push_back(polygon);

	polygon = new Poly(7);
	polygon->pts[0] = Point(400, 400);
	polygon->pts[1] = Point(250, 400);
	polygon->pts[2] = Point(100, 250);
	polygon->pts[3] = Point(100, 100);
	polygon->pts[4] = Point(600, 100);
	polygon->pts[5] = Point(500, 200);
	polygon->pts[6] = Point(500, 300);
	polygonsIn.push_back(polygon);
}

//to samo co sample2, ale i drugi odwrotnie zorientowany
void sample3()
{
	Poly *polygon = new Poly(5);
	polygon->pts[4] = Point(350, 50);
	polygon->pts[3] = Point(550, 450);
	polygon->pts[2] = Point(350, 250);
	polygon->pts[1] = Point(150, 450);
	polygon->pts[0] = Point(50, 350);
	polygonsIn.push_back(polygon);

	polygon = new Poly(7);
	polygon->pts[6] = Point(400, 400);
	polygon->pts[5] = Point(250, 400);
	polygon->pts[4] = Point(100, 250);
	polygon->pts[3] = Point(100, 100);
	polygon->pts[2] = Point(600, 100);
	polygon->pts[1] = Point(500, 200);
	polygon->pts[0] = Point(500, 300);
	polygonsIn.push_back(polygon);
}

//wsp�lna kraw�dz
void sample4()
{
	Poly *polygon = new Poly(4);
	polygon->pts[0] = Point(100,100);
	polygon->pts[1] = Point(250, 100);
	polygon->pts[2] = Point(250, 300);
	polygon->pts[3] = Point(100, 300);
	polygonsIn.push_back(polygon);

	polygon = new Poly(4);
	polygon->pts[0] = Point(100, 50);
	polygon->pts[1] = Point(300, 50);
	polygon->pts[2] = Point(300, 350);
	polygon->pts[3] = Point(100, 350);
	polygonsIn.push_back(polygon);
}

//rzeczyisty przypadek na kt�rym algorytm si� zap�tla�
void sample5()
{
	Poly *polygon = new Poly(11);
	polygon->pts[0] = Point(383, 378);
	polygon->pts[1] = Point(379, 384);
	polygon->pts[2] = Point(377, 391);
	polygon->pts[3] = Point(381, 397);
	polygon->pts[4] = Point(376, 402);
	polygon->pts[5] = Point(375, 395);
	polygon->pts[6] = Point(378, 389);
	polygon->pts[7] = Point(378, 388);
	polygon->pts[8] = Point(382, 394);
	polygon->pts[9] = Point(378, 400);
	polygon->pts[10] = Point(372, 403);
	polygonsIn.push_back(polygon);

	polygon = new Poly(15);
	polygon->pts[0] = Point(375, 405);
	polygon->pts[1] = Point(378, 400);
	polygon->pts[2] = Point(381, 392);
	polygon->pts[3] = Point(384, 387);
	polygon->pts[4] = Point(391, 384);
	polygon->pts[5] = Point(396, 389);
	polygon->pts[6] = Point(402, 392);
	polygon->pts[7] = Point(409, 391);
	polygon->pts[8] = Point(409, 391);
	polygon->pts[9] = Point(403, 395);
	polygon->pts[10] = Point(398, 400);
	polygon->pts[11] = Point(395, 405);
	polygon->pts[12] = Point(388, 408);
	polygon->pts[13] = Point(382, 404);
	polygon->pts[14] = Point(375, 402);
	polygonsIn.push_back(polygon);
}

//dwa wielok�ty stykaj� si� wierzcho�kiem - czy punkt przeci�cia liczony a� 4 razy?
void sample6()
{
	Poly *polygon = new Poly(4);
	polygon->pts[0] = Point(150,100);
	polygon->pts[1] = Point(250,200);
	polygon->pts[2] = Point(150,300);
	polygon->pts[3] = Point(100,200);
	polygonsIn.push_back(polygon);

	polygon = new Poly(4);
	polygon->pts[0] = Point(350,100);
	polygon->pts[1] = Point(400,200);
	polygon->pts[2] = Point(350,300);
	polygon->pts[3] = Point(250,200);
	polygonsIn.push_back(polygon);
}

//jeszcze b��d - zamiast sumy podaje lewy wielok�t
void sample7()
{
	//merged
	Poly *polygon = new Poly(85);
	polygon->pts[0] = Point(375, 185);
	polygon->pts[1] = Point(378, 180);
	polygon->pts[2] = Point(383, 177);
	polygon->pts[3] = Point(390, 175);
	polygon->pts[4] = Point(392, 168);
	polygon->pts[5] = Point(393, 162);
	polygon->pts[6] = Point(393, 162);
	polygon->pts[7] = Point(396, 157);
	polygon->pts[8] = Point(401, 152);
	polygon->pts[9] = Point(405, 148);
	polygon->pts[10] = Point(411, 147);
	polygon->pts[11] = Point(418, 145);
	polygon->pts[12] = Point(418, 145);
	polygon->pts[13] = Point(411, 144);
	polygon->pts[14] = Point(406, 147);
	polygon->pts[15] = Point(399, 148);
	polygon->pts[16] = Point(406, 146);
	polygon->pts[17] = Point(412, 144);
	polygon->pts[18] = Point(419, 142);
	polygon->pts[19] = Point(426, 142);
	polygon->pts[20] = Point(426, 142);
	polygon->pts[21] = Point(426, 141);
	polygon->pts[22] = Point(433, 141);
	polygon->pts[23] = Point(433, 142);
	polygon->pts[24] = Point(439, 144);
	polygon->pts[25] = Point(445, 147);
	polygon->pts[26] = Point(444, 148);
	polygon->pts[27] = Point(450, 149);
	polygon->pts[28] = Point(450, 155);
	polygon->pts[29] = Point(450, 160);
	polygon->pts[30] = Point(451, 161);
	polygon->pts[31] = Point(453, 165);
	polygon->pts[32] = Point(452, 160);
	polygon->pts[33] = Point(449, 159);
	polygon->pts[34] = Point(450, 160);
	polygon->pts[35] = Point(450, 162);
	polygon->pts[36] = Point(454, 167);
	polygon->pts[37] = Point(453, 165);
	polygon->pts[38] = Point(454, 166);
	polygon->pts[39] = Point(459, 171);
	polygon->pts[40] = Point(463, 175);
	polygon->pts[41] = Point(461, 181);
	polygon->pts[42] = Point(460, 175);
	polygon->pts[43] = Point(456, 174);
	polygon->pts[44] = Point(460, 177);
	polygon->pts[45] = Point(459, 183);
	polygon->pts[46] = Point(458, 190);
	polygon->pts[47] = Point(454, 192);
	polygon->pts[48] = Point(455, 191);
	polygon->pts[49] = Point(450, 194);
	polygon->pts[50] = Point(451, 194);
	polygon->pts[51] = Point(448, 197);
	polygon->pts[52] = Point(446, 199);
	polygon->pts[53] = Point(451, 195);
	polygon->pts[54] = Point(454, 192);
	polygon->pts[55] = Point(452, 193);
	polygon->pts[56] = Point(451, 194);
	polygon->pts[57] = Point(456, 194);
	polygon->pts[58] = Point(450, 197);
	polygon->pts[59] = Point(446, 202);
	polygon->pts[60] = Point(442, 206);
	polygon->pts[61] = Point(437, 210);
	polygon->pts[62] = Point(432, 214);
	polygon->pts[63] = Point(433, 221);
	polygon->pts[64] = Point(428, 217);
	polygon->pts[65] = Point(421, 216);
	polygon->pts[66] = Point(418, 218);
	polygon->pts[67] = Point(415, 223);
	polygon->pts[68] = Point(422, 226);
	polygon->pts[69] = Point(420, 232);
	polygon->pts[70] = Point(414, 230);
	polygon->pts[71] = Point(408, 231);
	polygon->pts[72] = Point(401, 232);
	polygon->pts[73] = Point(395, 232);
	polygon->pts[74] = Point(389, 233);
	polygon->pts[75] = Point(382, 233);
	polygon->pts[76] = Point(382, 227);
	polygon->pts[77] = Point(385, 221);
	polygon->pts[78] = Point(381, 217);
	polygon->pts[79] = Point(375, 215);
	polygon->pts[80] = Point(375, 209);
	polygon->pts[81] = Point(378, 203);
	polygon->pts[82] = Point(378, 197);
	polygon->pts[83] = Point(378, 198);
	polygon->pts[84] = Point(377, 192);
	polygonsIn.push_back(polygon);

	//bg->poly
	polygon = new Poly(23);
	polygon->pts[0] = Point(380, 178);
	polygon->pts[1] = Point(376, 184);
	polygon->pts[2] = Point(372, 188);
	polygon->pts[3] = Point(365, 190);
	polygon->pts[4] = Point(358, 189);
	polygon->pts[5] = Point(352, 187);
	polygon->pts[6] = Point(346, 184);
	polygon->pts[7] = Point(339, 182);
	polygon->pts[8] = Point(340, 176);
	polygon->pts[9] = Point(343, 169);
	polygon->pts[10] = Point(341, 162);
	polygon->pts[11] = Point(343, 156);
	polygon->pts[12] = Point(349, 153);
	polygon->pts[13] = Point(353, 148);
	polygon->pts[14] = Point(358, 144);
	polygon->pts[15] = Point(365, 143);
	polygon->pts[16] = Point(366, 150);
	polygon->pts[17] = Point(365, 157);
	polygon->pts[18] = Point(365, 163);
	polygon->pts[19] = Point(364, 170);
	polygon->pts[20] = Point(368, 175);
	polygon->pts[21] = Point(375, 175);
	polygon->pts[22] = Point(378, 180);
	polygonsIn.push_back(polygon);
}

//jeszcze nie koniec b��d�w
void sample8()
{
	//merged
	Poly *polygon = new Poly(114);
	polygon->pts[0] = Point(339, 182);
	polygon->pts[1] = Point(340, 176);
	polygon->pts[2] = Point(343, 169);
	polygon->pts[3] = Point(341, 162);
	polygon->pts[4] = Point(343, 156);
	polygon->pts[5] = Point(349, 153);
	polygon->pts[6] = Point(353, 148);
	polygon->pts[7] = Point(358, 144);
	polygon->pts[8] = Point(365, 143);
	polygon->pts[9] = Point(366, 150);
	polygon->pts[10] = Point(365, 157);
	polygon->pts[11] = Point(365, 163);
	polygon->pts[12] = Point(364, 170);
	polygon->pts[13] = Point(368, 175);
	polygon->pts[14] = Point(375, 175);
	polygon->pts[15] = Point(378, 180);
	polygon->pts[16] = Point(378, 180);
	polygon->pts[17] = Point(379, 179);
	polygon->pts[18] = Point(376, 184);
	polygon->pts[19] = Point(375, 185);
	polygon->pts[20] = Point(378, 180);
	polygon->pts[21] = Point(378, 180);
	polygon->pts[22] = Point(380, 178);
	polygon->pts[23] = Point(379, 179);
	polygon->pts[24] = Point(383, 177);
	polygon->pts[25] = Point(390, 175);
	polygon->pts[26] = Point(392, 168);
	polygon->pts[27] = Point(393, 162);
	polygon->pts[28] = Point(393, 162);
	polygon->pts[29] = Point(396, 157);
	polygon->pts[30] = Point(401, 152);
	polygon->pts[31] = Point(405, 148);
	polygon->pts[32] = Point(411, 147);
	polygon->pts[33] = Point(418, 145);
	polygon->pts[34] = Point(418, 145);
	polygon->pts[35] = Point(411, 144);
	polygon->pts[36] = Point(406, 147);
	polygon->pts[37] = Point(399, 148);
	polygon->pts[38] = Point(406, 146);
	polygon->pts[39] = Point(412, 144);
	polygon->pts[40] = Point(419, 142);
	polygon->pts[41] = Point(426, 142);
	polygon->pts[42] = Point(426, 142);
	polygon->pts[43] = Point(426, 141);
	polygon->pts[44] = Point(433, 141);
	polygon->pts[45] = Point(433, 142);
	polygon->pts[46] = Point(439, 144);
	polygon->pts[47] = Point(445, 147);
	polygon->pts[48] = Point(444, 148);
	polygon->pts[49] = Point(450, 149);
	polygon->pts[50] = Point(450, 155);
	polygon->pts[51] = Point(450, 160);
	polygon->pts[52] = Point(451, 161);
	polygon->pts[53] = Point(453, 165);
	polygon->pts[54] = Point(452, 160);
	polygon->pts[55] = Point(449, 159);
	polygon->pts[56] = Point(450, 160);
	polygon->pts[57] = Point(450, 162);
	polygon->pts[58] = Point(454, 167);
	polygon->pts[59] = Point(453, 165);
	polygon->pts[60] = Point(454, 166);
	polygon->pts[61] = Point(459, 171);
	polygon->pts[62] = Point(463, 175);
	polygon->pts[63] = Point(461, 181);
	polygon->pts[64] = Point(460, 175);
	polygon->pts[65] = Point(456, 174);
	polygon->pts[66] = Point(460, 177);
	polygon->pts[67] = Point(459, 183);
	polygon->pts[68] = Point(458, 190);
	polygon->pts[69] = Point(454, 192);
	polygon->pts[70] = Point(455, 191);
	polygon->pts[71] = Point(450, 194);
	polygon->pts[72] = Point(451, 194);
	polygon->pts[73] = Point(448, 197);
	polygon->pts[74] = Point(446, 199);
	polygon->pts[75] = Point(451, 195);
	polygon->pts[76] = Point(454, 192);
	polygon->pts[77] = Point(452, 193);
	polygon->pts[78] = Point(451, 194);
	polygon->pts[79] = Point(456, 194);
	polygon->pts[80] = Point(450, 197);
	polygon->pts[81] = Point(446, 202);
	polygon->pts[82] = Point(442, 206);
	polygon->pts[83] = Point(437, 210);
	polygon->pts[84] = Point(432, 214);
	polygon->pts[85] = Point(433, 221);
	polygon->pts[86] = Point(428, 217);
	polygon->pts[87] = Point(421, 216);
	polygon->pts[88] = Point(418, 218);
	polygon->pts[89] = Point(415, 223);
	polygon->pts[90] = Point(422, 226);
	polygon->pts[91] = Point(420, 232);
	polygon->pts[92] = Point(414, 230);
	polygon->pts[93] = Point(408, 231);
	polygon->pts[94] = Point(401, 232);
	polygon->pts[95] = Point(395, 232);
	polygon->pts[96] = Point(389, 233);
	polygon->pts[97] = Point(382, 233);
	polygon->pts[98] = Point(382, 227);
	polygon->pts[99] = Point(385, 221);
	polygon->pts[100] = Point(381, 217);
	polygon->pts[101] = Point(375, 215);
	polygon->pts[102] = Point(375, 209);
	polygon->pts[103] = Point(378, 203);
	polygon->pts[104] = Point(378, 197);
	polygon->pts[105] = Point(378, 198);
	polygon->pts[106] = Point(377, 192);
	polygon->pts[107] = Point(375, 185);
	polygon->pts[108] = Point(375, 185);
	polygon->pts[109] = Point(372, 188);
	polygon->pts[110] = Point(365, 190);
	polygon->pts[111] = Point(358, 189);
	polygon->pts[112] = Point(352, 187);
	polygon->pts[113] = Point(346, 184);
	polygonsIn.push_back(polygon);

	//bg->poly
	polygon = new Poly(12);
	polygon->pts[0] = Point(374, 185);
	polygon->pts[1] = Point(368, 188);
	polygon->pts[2] = Point(362, 189);
	polygon->pts[3] = Point(355, 189);
	polygon->pts[4] = Point(349, 186);
	polygon->pts[5] = Point(343, 182);
	polygon->pts[6] = Point(344, 183);
	polygon->pts[7] = Point(350, 187);
	polygon->pts[8] = Point(357, 189);
	polygon->pts[9] = Point(364, 191);
	polygon->pts[10] = Point(371, 189);
	polygon->pts[11] = Point(375, 185);
	polygonsIn.push_back(polygon);
}


//uproszczone sample8
void sample9()
{
	//merged
	Poly *polygon = new Poly(24);
	polygon->pts[0] = Point(339, 182);
	polygon->pts[1] = Point(340, 176);
	polygon->pts[2] = Point(343, 169);
	polygon->pts[3] = Point(364, 170);
	polygon->pts[4] = Point(368, 175);
	polygon->pts[5] = Point(375, 175);
	polygon->pts[6] = Point(379, 179);
	polygon->pts[7] = Point(376, 184);
	polygon->pts[8] = Point(375, 185);
	polygon->pts[9] = Point(378, 180);
	polygon->pts[10] = Point(380, 178);
	polygon->pts[11] = Point(379, 179);
	polygon->pts[12] = Point(383, 177);
	polygon->pts[13] = Point(390, 175);
	polygon->pts[14] = Point(378, 203);
	polygon->pts[15] = Point(378, 197);
	polygon->pts[16] = Point(378, 198);
	polygon->pts[17] = Point(377, 192);
	polygon->pts[18] = Point(375, 185);
	polygon->pts[19] = Point(372, 188);
	polygon->pts[20] = Point(365, 190);
	polygon->pts[21] = Point(358, 189);
	polygon->pts[22] = Point(352, 187);
	polygon->pts[23] = Point(346, 184);
	polygonsIn.push_back(polygon);

	//bg->poly
	polygon = new Poly(12);
	polygon->pts[0] = Point(374, 185);
	polygon->pts[1] = Point(368, 188);
	polygon->pts[2] = Point(362, 189);
	polygon->pts[3] = Point(355, 189);
	polygon->pts[4] = Point(349, 186);
	polygon->pts[5] = Point(343, 182);
	polygon->pts[6] = Point(344, 183);
	polygon->pts[7] = Point(350, 187);
	polygon->pts[8] = Point(357, 189);
	polygon->pts[9] = Point(364, 191);
	polygon->pts[10] = Point(371, 189);
	polygon->pts[11] = Point(375, 185);
	polygonsIn.push_back(polygon);
}

//ca�kiem ma�o z�o��iwy przypadek, kt�ry pokazuje �e jest ��e gdy przyjmujemy krytertium
//zmiany wielok�ta przy punkcie przeci�cia
//�le jest gdy ten kt�rym idziemy tylko dotyka a potem zawraca, czyli nast�pny wierzcho�ek
//po tej samej stronie
void sample10()
{
	Poly *polygon = new Poly(9);
	polygon->pts[0] = Point(150,50);
	polygon->pts[1] = Point(350,50);
	polygon->pts[2] = Point(350,100);
	polygon->pts[3] = Point(360,50);
	polygon->pts[4] = Point(450,50);
	polygon->pts[5] = Point(450,250);
	polygon->pts[6] = Point(350,150);
	polygon->pts[7] = Point(250,250);
	polygon->pts[8] = Point(150,150);
	polygonsIn.push_back(polygon);

	polygon = new Poly(4);
	polygon->pts[0] = Point(250,150);
	polygon->pts[1] = Point(370,90); //punkt zawracania
	polygon->pts[2] = Point(300,250);
	polygon->pts[3] = Point(250,250);
	polygonsIn.push_back(polygon);
}

//trudniejsza wersja - dotyka wierzcho�ka
void sample10a()
{
	Poly *polygon = new Poly(9);
	polygon->pts[0] = Point(150,50);
	polygon->pts[1] = Point(350,50);
	polygon->pts[2] = Point(350,100);
	polygon->pts[3] = Point(360,50);
	polygon->pts[4] = Point(450,50);
	polygon->pts[5] = Point(450,250);
	polygon->pts[6] = Point(350,150);
	polygon->pts[7] = Point(250,250);
	polygon->pts[8] = Point(150,150);
	polygonsIn.push_back(polygon);

	polygon = new Poly(4);
	polygon->pts[0] = Point(250,150);
	polygon->pts[1] = Point(350,100); //trudne - dotyka tego punktu
	polygon->pts[2] = Point(300,250);
	polygon->pts[3] = Point(250,250);
	polygonsIn.push_back(polygon);
}

//przypadek zdegenerowany - wszystkie punkty wsp�liniowe
//0->1->2 pierwszego idzie w tym samy mierunku co 0->1 wi�c nie zmieniamy
//wielok�ta, gdy w praeciwnym - zmieniamy
void sample11()
{
	Poly *polygon = new Poly(3);
	polygon->pts[0] = Point(50,100);
	polygon->pts[1] = Point(150,100);
	polygon->pts[2] = Point(250,100);
	polygonsIn.push_back(polygon);

	polygon = new Poly(3);
	polygon->pts[0] = Point(100,100);
	polygon->pts[1] = Point(200,100);
	polygon->pts[2] = Point(150,200);
	polygonsIn.push_back(polygon);
}

//kraw�dzie jedego wielok�ta le�� na kraw�ziach innego
//trzeba wybra�, czy zmieni� wielok�t czy pozostawi� bez zmian
void sample12a()
{
	Poly *polygon;
	polygon = new Poly(6);
	polygon->pts[0] = Point(50,100);
	polygon->pts[1] = Point(150,100);
	polygon->pts[2] = Point(200,50);
	polygon->pts[3] = Point(200,200);
	polygon->pts[4] = Point(150,250);
	polygon->pts[5] = Point(50,250);
	polygonsIn.push_back(polygon);

	polygon = new Poly(4);
	polygon->pts[0] = Point(50,100);
	polygon->pts[1] = Point(250,100);
	polygon->pts[2] = Point(250,250);
	polygon->pts[3] = Point(50,250);
	polygonsIn.push_back(polygon);
}

//zamienione wielok�ty
void sample12b()
{
	Poly *polygon;
	polygon = new Poly(4);
	polygon->pts[0] = Point(50,100);
	polygon->pts[1] = Point(250,100);
	polygon->pts[2] = Point(250,250);
	polygon->pts[3] = Point(50,250);
	polygonsIn.push_back(polygon);

	polygon = new Poly(6);
	polygon->pts[0] = Point(50,100);
	polygon->pts[1] = Point(150,100);
	polygon->pts[2] = Point(200,50);
	polygon->pts[3] = Point(200,200);
	polygon->pts[4] = Point(150,250);
	polygon->pts[5] = Point(50,250);
	polygonsIn.push_back(polygon);
}

void sample13()
{
	Poly *polygon;
	polygon = new Poly(10);
	polygon->pts[0] = Point(100,50);
	polygon->pts[1] = Point(200,150);
	polygon->pts[2] = Point(250,150);
	polygon->pts[3] = Point(300,150);
	polygon->pts[4] = Point(400,50);
	polygon->pts[5] = Point(400,400);
	polygon->pts[6] = Point(300,300);
	polygon->pts[7] = Point(250,300);
	polygon->pts[8] = Point(200,300);
	polygon->pts[9] = Point(100,200);
	polygonsIn.push_back(polygon);

	polygon = new Poly(4);
	polygon->pts[0] = Point(150,150);
	polygon->pts[1] = Point(350,150);
	polygon->pts[2] = Point(350,300);
	polygon->pts[3] = Point(150,300);
	polygonsIn.push_back(polygon);
}

Poly PolyOut;

void testAll()
{
	sample0();
	WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, true);
	delete polygonsIn[0];
	delete polygonsIn[1];
	polygonsIn.clear();

	sample1();
	WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, true);
	delete polygonsIn[0];
	delete polygonsIn[1];
	polygonsIn.clear();

	sample2();
	WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, true);
	delete polygonsIn[0];
	delete polygonsIn[1];
	polygonsIn.clear();

	sample3();
	WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, true);
	delete polygonsIn[0];
	delete polygonsIn[1];
	polygonsIn.clear();

	sample4();
	WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, true);
	delete polygonsIn[0];
	delete polygonsIn[1];
	polygonsIn.clear();

	sample5();
	WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, true);
	delete polygonsIn[0];
	delete polygonsIn[1];
	polygonsIn.clear();

	sample6();
	WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, true);
	delete polygonsIn[0];
	delete polygonsIn[1];
	polygonsIn.clear();

	sample7();
	WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, true);
	delete polygonsIn[0];
	delete polygonsIn[1];
	polygonsIn.clear();

	sample8();
	WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, true);
	delete polygonsIn[0];
	delete polygonsIn[1];
	polygonsIn.clear();

	sample9();
	WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, true);
	delete polygonsIn[0];
	delete polygonsIn[1];
	polygonsIn.clear();

	sample10();
	WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, true);
	delete polygonsIn[0];
	delete polygonsIn[1];
	polygonsIn.clear();

	sample10a();
	WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, true);
	delete polygonsIn[0];
	delete polygonsIn[1];
	polygonsIn.clear();

	sample11();
	WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, true);
	delete polygonsIn[0];
	delete polygonsIn[1];
	polygonsIn.clear();

	sample12a();
	WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, true);
	delete polygonsIn[0];
	delete polygonsIn[1];
	polygonsIn.clear();

	sample12b();
	WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, true);
	delete polygonsIn[0];
	delete polygonsIn[1];
	polygonsIn.clear();

	sample13();
	WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, true);
	delete polygonsIn[0];
	delete polygonsIn[1];
	polygonsIn.clear();
}

void crash()
{
	sample8();
	WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, true);
	delete polygonsIn[0];
	delete polygonsIn[1];
	polygonsIn.clear();
}
void benchmark()
{
	LARGE_INTEGER timeStart, timeEnd, freq;
	QueryPerformanceCounter(&timeStart);
	QueryPerformanceFrequency(&freq);
	for (int i = 0; i < 1000; i++)
	{
		sample7();
		WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, false);
		delete polygonsIn[0];
		delete polygonsIn[1];
		polygonsIn.clear();
		sample8();
		WeilerAtherton(polygonsIn[0], polygonsIn[1], &PolyOut, false);
		delete polygonsIn[0];
		delete polygonsIn[1];
		polygonsIn.clear();
	}
	QueryPerformanceCounter(&timeEnd);
	printf("czas=%f ms\n", (double)(timeEnd.QuadPart - timeStart.QuadPart) / freq.QuadPart * 1000);
}

int _tmain(int argc, _TCHAR* argv[])
{
	//testAll();
	crash();
	//benchmark();
	return 0;
}
//1000 razy sample7+sample8: 181 ms
//najpierw brute force, potem korzysta: 182 ms
//ostatnio 184 ms
//wychodzenie przy te�cie segmentu - ju� tylko 86