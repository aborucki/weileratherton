#include "Debug.h"

void scalePoints(Point &p0, Point &p1)
{
	return;
	p0.x = 15 * (p0.x - 350);
	p0.y = 15 * (p0.y - 370);
	p1.x = 15 * (p1.x - 350);
	p1.y = 15 * (p1.y - 370);
}

void drawPoly(Mat mat, Poly *polygon, Scalar color)
{
	for (int j = 0; j<polygon->n; j++)
	{
		Point p0, p1;
		p0 = polygon->pts[j];
		p1 = polygon->pts[(j + 1) % polygon->n];
		//powi�kszenie do cel�w testowych
		scalePoints(p0, p1);
		line(mat, p0, p1, color, 1, 1, 0);
		//std::string str;
		//str = std::to_string((long long)j);
		//putText(mat, str.c_str(), p0,0,0.3,color);
		//imshow("mat", mat);
		//cvWaitKey(0);
	}
}
